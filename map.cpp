#include <string>
#include "map.h"
#include "./ui-element.h"

Map::Map (int windowWidth, int windowHeight, const std::string map[], float playerPosition[], int x, int y) : UiElement(windowWidth, windowHeight, x, y) {
	Map::map = map;
	Map::playerPosition = playerPosition;
}

void Map::Draw (wchar_t* buffer) {
	// Draw the map
	for (size_t i = 0; i < map->length() - 1; i++) {
		for (size_t y = 0; y < map[i].length(); y++) {
			buffer[position + i * windowWidth + y] = map[i][y];
		}
	}

	// Overwrite the map coordinates of the player's position.
	buffer[position + (size_t)playerPosition[0] * windowWidth + (size_t)playerPosition[1]] = playerSymbol;
}
