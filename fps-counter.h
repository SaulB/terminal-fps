#include <chrono>
#include "ui-element.h"

#ifndef FPSCOUNTER_H
#define FPSCOUNTER_H

class FpsCounter : UiElement {
	public:
		FpsCounter(int windowWidth, int windowHeight, int x = 0, int y = 0);
		void Draw(wchar_t* buffer);
		int GetFPS();

	private:
		int fps = 1;
		std::chrono::_V2::system_clock::time_point lastFrameTime;
};

#endif /* FPSCOUNTER_H */
