#include <unistd.h>
#include <math.h>
#include <string>
#include "fps-counter.h"
#include "text-label.h"
#include "map.h"
#include "input.h"
#include "render.h"

const float FOV = 3.14159 / 5.0;

const float TURNSPEED = 0.03;
const float MOVESPEED = 0.1;

const std::string MAP[] = {
	"################",
	"#              #",
	"#              #",
	"#              #",
	"######         #",
	"#    #         #",
	"#    #         #",
	"#    #         #",
	"#    #         #",
	"#    #    #    #",
	"#    #    #    #",
	"#         #    #",
	"#         #    #",
	"#         #    #",
	"################"
};

float playerAngle = 0.0;
float playerPosition[] = { 2.0, 2.0 };

bool quit = false;

// Function to be passed to the input object.
void onInput(char c) {
	if (c == 'd')
		playerAngle += TURNSPEED;

	if (c == 'a')
		playerAngle -= TURNSPEED;

	if (c == 'w') {
		float newPosX = playerPosition[0] + sin(playerAngle) * MOVESPEED;
		float newPosY = playerPosition[1] + cos(playerAngle) * MOVESPEED;

		//if (MAP[(int)newPosY][(int)newPosX] != WALLSYMBOL) {
			playerPosition[0] = newPosX;
			playerPosition[1] = newPosY;
		//}
	}

	if (c == 's') {
		float newPosX = playerPosition[0] - sin(playerAngle) * MOVESPEED;
		float newPosY = playerPosition[1] - cos(playerAngle) * MOVESPEED;

		//if (MAP[(int)newPosY][(int)newPosX] != WALLSYMBOL) {
			playerPosition[0] = newPosX;
			playerPosition[1] = newPosY;
		//}
	}

	// update the flag so the program can quit itself.
	if (c == 'q')
		quit = true;
}

int main ()  {
	// Create a new renderer
	Render renderer(
		MAP,
		sizeof(MAP) / sizeof(*MAP),
		playerPosition,
		playerAngle,
		FOV
	);

	// Create a fps counter to track stats
	FpsCounter fpsCounter(renderer.GetWidth(), renderer.GetHeight());

	// Labels to tell the user the controls
	std::string labelString = "'wasd' to turn and move, 'q' to quit";
	TextLabel controlLabel(
		renderer.GetWidth(),
		renderer.GetHeight(),
		labelString,
		renderer.GetWidth() - labelString.length()
	);

	// Create a map to track the player
	Map map(renderer.GetWidth(), renderer.GetHeight(), MAP, playerPosition);

	// Handle input
	Input input;

	input.OnKey = &onInput;
	input.StartListening();

	while (true) {
		// update the renderer view
		renderer.Update();

		// Draw the ui
		fpsCounter.Draw(renderer.GetDisplayBuffer());
		map.Draw(renderer.GetDisplayBuffer());
		controlLabel.Draw(renderer.GetDisplayBuffer());

		// Display the output to the terminal
		renderer.Display();

		// Flush and rest
		fflush(stdout);

		usleep(30); // unlimited but give the system a little yeild time.
		//usleep(16666); // 60 fps MAX

		if (quit)
			break;
	}

	return 0;
}
