#include <termios.h>
#include <stdio.h>
#include <thread>
#include <unistd.h>
#include "input.h"

Input::Input() {
	tcgetattr(0, &old); /* grab old terminal i/o settings */
	current = old; /* make new settings same as old settings */
	current.c_lflag &= ~ICANON; /* disable buffered i/o */

	current.c_lflag &= ~ECHO; /* set no echo mode */

	tcsetattr(0, TCSANOW, &current); /* use these new terminal i/o settings now */
}

Input::~Input() {
	tcsetattr(0, TCSANOW, &old);
}

void Input::StartListening() {
	inputThread = new std::thread([&]() {
		while (true) {
			usleep(30);

			char c = getchar();

			(*Input::OnKey)(c);
		}
	});
}
