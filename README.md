# terminal-fps

A first person engine that renders in the terminal.

![Starting Screenshot](screenshots/screenshot-1.png)
![Angle Screenshot](screenshots/screenshot-2.png)

## Controls
Use the 'a', and 'd' keys to turn.<br />
Use the 'w' and 's' keys to move.<br />
Press 'q' to quit.

## Running
To compile simply run:
```
make
```

And to run it:
```
./main
```
