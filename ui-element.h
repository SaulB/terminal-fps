#ifndef UIELEMENT_H
#define UIELEMENT_H

class UiElement {
	public:
		UiElement (int windowWidth, int windowHeight, int x = 0, int y = 0);

		virtual void Draw(wchar_t* buffer) = 0;


	protected:
		int windowWidth;
		int windowHeight;
		int x;
		int y;
		int position = 0;
};

#endif /* UIELEMENT_H */
