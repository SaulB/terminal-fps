#include <sys/ioctl.h>
#include <string>

#ifndef RENDER_H
#define RENDER_H

class Render {
	public:
		Render(const std::string map[], int mapHeight, float playerPosition[], float &playerAngle, float fov);
		~Render();
		wchar_t* GetDisplayBuffer();
		void Display();
		void Update();
		int GetWidth();
		int GetHeight();
		int GetLength();

		wchar_t space = ' ';
		wchar_t wall[4] = { 0x2588, 0x2593, 0x2592, 0x2591 };
		wchar_t floor[4] = { '#', 'x', '.', '-' };
		char boundry = '|';
		char wallSymbol = '#';
		int maxRenderDistance = 17;

	private:
		struct winsize ws;
		const std::string* map;
		int mapHeight;
		wchar_t* dispalyBuffer;
		float* playerPosition;
		float &playerAngle;
		float fov;
};

#endif /* RENDER_H */
