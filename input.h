#include <thread>
#include <termios.h>

#ifndef INPUT_H
#define INPUT_H

class Input {
	public:
		Input();
		~Input();
		void (*OnKey)(char);
		void StartListening();

	private:
		struct termios old, current;
		std::thread *inputThread;
};

#endif /* INPUT_H */
