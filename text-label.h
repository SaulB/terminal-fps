#include <string>
#include "ui-element.h"

#ifndef TEXTLABEL_H
#define TEXTLABEL_H

class TextLabel : UiElement {
	public:
		TextLabel(int windowWidth, int windowHeight, std::string text, int x = 0, int y = 0);
		void Draw(wchar_t* buffer);
		void ChangeText(std::string text);

	private:
		std::string text;
};

#endif /* TEXTLABEL_H */
