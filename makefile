CXX = g++
CPPFLAGS = -g -Wall -std=c++11
LDFLAGS = -pthread

main: main.o ui-element.o fps-counter.o map.o input.o render.o text-label.o
	$(CXX) $(LDFLAGS) -o main main.o fps-counter.o ui-element.o map.o input.o render.o text-label.o

clean:
	rm main *.o
