#include <string>
#include "ui-element.h"

#ifndef MAP_H
#define MAP_H

class Map : UiElement {
	public:
		Map(int windowWidth, int windowHeight, const std::string map[], float playerPosition[], int x = 0, int y = 1);
		void Draw(wchar_t* buffer);
		char playerSymbol = '*';

	private:
		const std::string* map;
		float* playerPosition;
};

#endif /* MAP_H */
