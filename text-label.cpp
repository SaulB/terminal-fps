#include <string>
#include <assert.h>
#include "text-label.h"

TextLabel::TextLabel(int windowWidth, int windowHeight, std::string text, int x, int y)
: UiElement(windowWidth, windowHeight, x, y) {
	assert((signed)text.length() + position <= windowWidth);

	TextLabel::text = text;
}

void TextLabel::ChangeText (std::string text) {
	TextLabel::text = text;
}

void TextLabel::Draw(wchar_t* buffer) {
	for(size_t i = 0; i < text.length(); i++) {
		buffer[position + i] = text[i];
	}
}
