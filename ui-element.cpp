#include <assert.h>
#include "ui-element.h"

UiElement::UiElement(int windowWidth, int windowHeight, int x, int y) {
	assert(y >= 0);
	assert(x >= 0);
	assert(y <= windowHeight);
	assert(x <= windowWidth);

	UiElement::windowWidth = windowWidth;
	UiElement::windowHeight = windowHeight;
	UiElement::x = x;
	UiElement::y = y;

	position = windowWidth * y + x;
}
