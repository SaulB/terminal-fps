#include <string>
#include <sys/time.h>
#include <chrono>
#include "fps-counter.h"
#include "ui-element.h"

typedef std::chrono::high_resolution_clock Clock;

FpsCounter::FpsCounter(int windowWidth, int windowHeight, int x, int y)
: UiElement(windowWidth, windowHeight, x, y) {
	//Setup the first frame time (called last because it was the last used).
	lastFrameTime = Clock::now();
}

void FpsCounter::Draw(wchar_t* buffer) {
	// Calculate elapsed time since the last call
	std::chrono::_V2::system_clock::time_point thisFrameTime = Clock::now();

	int elapsedMicroSecs = std::chrono::duration_cast<std::chrono::microseconds>(thisFrameTime - lastFrameTime).count();

	fps = 1000000 / elapsedMicroSecs;

	// Build the string for output
	std::string output = "FPS: ";

	output += std::to_string(fps);

	// Output the whole string
	for(size_t i = 0; i < output.length(); i++) {
		buffer[position + i] = output[i];
	}

	// Update our last displayed frame time.
	lastFrameTime = Clock::now();
}

int FpsCounter::GetFPS() {
	return fps;
}
