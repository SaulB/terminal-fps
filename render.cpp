#include <string>
#include <sys/ioctl.h>
#include <unistd.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include "render.h"

Render::Render (const std::string map[], int mapHeight, float playerPosition[], float& angle, float fov) : playerAngle(angle) {
	Render::map = map;
	Render::playerPosition = playerPosition;
	Render::fov = fov;
	Render::mapHeight = mapHeight;

	// setup locale
	setlocale(LC_ALL, "");

	ioctl (STDOUT_FILENO, TIOCGWINSZ, &ws);

	// Disable the cursor.
	system("setterm -cursor off");

	// Initialize with clear space
	dispalyBuffer = new wchar_t[ws.ws_row * ws.ws_col];

	for (int i = 0; i < ws.ws_row * ws.ws_col; i++) {
		dispalyBuffer[i] = space;
	}
}

Render::~Render() {
	system("setterm -cursor on");
}

wchar_t* Render::GetDisplayBuffer () {
	return dispalyBuffer;
}

void Render::Display () {
	char* cursorToTop = new char[ws.ws_row * 3];

	// Set the cursor to the top.
	for (int i = 0; i < (ws.ws_row - 1) * 3;) {
		cursorToTop[i++] = '\x1b';
		cursorToTop[i++] = '[';
		cursorToTop[i++] = 'A';
	}

	// Print the display buffer from the cursor at the top.
	printf("%s\r%ls", cursorToTop, dispalyBuffer);
}

int Render::GetWidth() {
	return ws.ws_col;
}

int Render::GetHeight() {
	return ws.ws_row;
}

int Render::GetLength() {
	return ws.ws_col * ws.ws_row;
}

void Render::Update () {
	for (int x = 0; x < ws.ws_col; x++) {
		float rayAngle = (playerAngle - fov / 2.0) + (float(x) / float(ws.ws_col)) * fov;

		float distanceToWall = 0.0;
		float eyeX = sin(rayAngle);
		float eyeY = cos(rayAngle);
		bool hitWall = false;

		while (!hitWall && distanceToWall < maxRenderDistance) {
			distanceToWall += 0.1;

			bool boundry = false;

			int testX = playerPosition[0] + eyeX * distanceToWall;
			int testY = playerPosition[1] + eyeY * distanceToWall;

			if (testX < 0 || testX >= (signed)map[0].length() || testY < 0 || testY >= mapHeight + 1) {
				hitWall = true;
				distanceToWall = maxRenderDistance;
			} else {
				if (map[testX][testY] == wallSymbol) {
					hitWall = true;

					std::vector<std::pair<float, float>> p;

					for (int tx = 0; tx < 2; tx++) {
						for (int ty = 0; ty < 2; ty++) {
							float vy = (float)testY + ty - playerPosition[1];
							float vx = (float)testX + tx - playerPosition[0];
							float d = sqrt(vx * vx + vy * vy);
							float dot = (eyeX * vx / d) + (eyeY * vy / d);

							p.push_back(std::make_pair(d, dot));
						}
					}

					std::sort(p.begin(), p.end(), [](const std::pair<float, float> &left, const std::pair<float, float> &right) { return left.first < right.first; });

					float bound = 0.005;

					for (int x = 0; x < 2; x++) {
						if (acos(p.at(x).second) < bound)
							boundry = true;
					}
				}
			}

			int ceilingRow = (ws.ws_row / 2.0) - ws.ws_row / distanceToWall;
			int floorRow = ws.ws_row - ceilingRow;

			// Wall shading
			short wallShade = ' ';

			int wallSymbolCount = sizeof(wall) / sizeof(*wall);

			for (int y = 0; y < wallSymbolCount; y++) {
				if (distanceToWall <= maxRenderDistance / (wallSymbolCount - y)){
					wallShade = wall[y];
					break;
				}
			}

			// Boundry shading
			if (boundry)
				wallShade = Render::boundry;

			for (int y = 0; y < ws.ws_row; y++) {
				if (y < ceilingRow) {
					dispalyBuffer[y * ws.ws_col + x] = ' ';
				} else if (y > ceilingRow && y <= floorRow) {
					dispalyBuffer[y * ws.ws_col + x] = wallShade;
				} else {
					short floorShade = ' ';

					int floorSymbolCount = sizeof(floor) / sizeof(*floor);

					float b = 1.0f - (((float)y - ws.ws_row / 2.0f) / ((float)ws.ws_row / 2.0f));

					// Check what shade to use for the floor
					for (int z = 0; z < floorSymbolCount; z++) {
						if (b < float(z + 1) / float(floorSymbolCount + 1)){
							floorShade = floor[z];
							break;
						}
					}

					dispalyBuffer[y * ws.ws_col + x] = floorShade;
				}
			}
		}
	}
}
